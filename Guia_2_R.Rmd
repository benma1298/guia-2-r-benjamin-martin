---
title: "Guia 2 Agua potable"
author: "Benjamin Martin"
date: "28-06-2021"
output: 
  pdf_document:
    highlight: zenburn
    toc: yes
    number_sections: true
    toc_depth: 2
    latex_engine: pdflatex
  html_document:
    highlight: default
    number_sections: true
    theme: cosmo
    toc: yes
    toc_depth: 2
  word_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Agua Potable

## Carga dataset
```{r}
dataset <- "water_potability.csv"
data <- read.csv(dataset, sep=",")
```

### Nombre de las columnas o nombre de las variables
```{r}
colnames(data)
names(data)
```
### Dimension del dataset
```{r}
dim(data)
```

### Estructura dataset
```{r}
str(data)
```

### Resumen basico estadistico
```{r}
summary(data)
```

### Identificar y mostrar atributos, variables o columnas cualitativas y cuantitativas

* Cualitativo: Caractristicas o atributos.
* Cuantitativo: Cantidades.

```{r}
str(data)
```
```{r}
summary(data)
```

### Cualitativas a factor
```{r}
data$city <- as.factor(data$city)
data$area <- as.factor(data$area)
```

```{r}
summary(data)
```

## Separar dataset en grupos de datos

### Uso de subset
```{r}
Maule <- subset(data, city == c("Curicó", "Linares", "Talca"))
Otra <- subset(data, city == c("Chillan", "Rancagua", "Santiago", "Temuco"))

# resumen

head(Maule)
tail(Maule)

head(Otra)
tail(Otra)
```

### Correccion tipo y formato variables cuantitativas
```{r}
data$time.emergency <- as.numeric(data$time.emergency)
data$type.emergency <- as.numeric(data$type.emergency)
data$Potability <- as.numeric(data$Potability)
```

```{r}
str(data)
```

```{r}
summary(data)
```

## Realizar graficos sobre variables cuantitativas

### Variables cuantitativas
```{r}
print("Histogramas")

par(mfrow=c(1,2))
hist(x=Maule$time.emergency, main="Tiempo de emergencia, Maule",
     xlab="Tiempo de emergencia", ylab="Frecuencia")
hist(x=Otra$time.emergency, main="Tiempo de emergencia, Otra region",
     xlab="Tiempo de emergencia", ylab="Frecuencia")

par(mfrow=c(1,2))
hist(x=Maule$type.emergency, main="Tipo de emergencia, Maule",
     xlab="Tipo de emergencia", ylab="Frecuencia")
hist(x=Otra$type.emergency, main="Tipo de emergencia, Otra region",
     xlab="Tipo de emergencia", ylab="Frecuencia")

par(mfrow=c(1,2))
hist(x=Maule$ph, main="pH del agua, Maule",
     xlab="pH", ylab="Frecuencia")
hist(x=Otra$ph, main="pH del agua, Otra region",
     xlab="pH", ylab="Frecuencia")

par(mfrow=c(1,2))
hist(x=Maule$Hardness, main="Dureza del agua, Maule",
     xlab="Dureza", ylab="Frecuencia")
hist(x=Otra$Hardness, main="Dureza del agua, Otra region",
     xlab="Dureza", ylab="Frecuencia")

par(mfrow=c(1,2))
hist(x=Maule$Solids, main="Solidos en el agua, Maule",
     xlab="Solidos", ylab="Frecuencia")
hist(x=Otra$Hardness, main="Solidos en el agua, Otra region",
     xlab="Solidos", ylab="Frecuencia")

par(mfrow=c(1,2))
hist(x=Maule$Chloramines, main="Cloro en el agua, Maule",
     xlab="Cloro", ylab="Frecuencia")
hist(x=Otra$Chloramines, main="Cloro en el agua, Otra region",
     xlab="Cloro", ylab="Frecuencia")

par(mfrow=c(1,2))
hist(x=Maule$Sulfate, main="Sulfato en el agua, Maule",
     xlab="Sulfato", ylab="Frecuencia")
hist(x=Otra$Sulfate, main="Sulfato en el agua, Otra region",
     xlab="Sulfato", ylab="Frecuencia")

par(mfrow=c(1,2))
hist(x=Maule$Conductivity, main="Conductividad el agua, Maule",
     xlab="Conductividad", ylab="Frecuencia")
hist(x=Otra$Conductivity, main="Conductividad del agua, Otra region",
     xlab="Conductividad", ylab="Frecuencia")

par(mfrow=c(1,2))
hist(x=Maule$Organic_carbon, main="Carbon organico en el agua, Maule",
     xlab="Carbono organico", ylab="Frecuencia")
hist(x=Otra$Organic_carbon, main="Carbono organico en el agua, Otra region",
     xlab="Carbono organico", ylab="Frecuencia")


par(mfrow=c(1,2))
hist(x=Maule$Trihalomethanes, main="THM en el agua, Maule",
     xlab="THM", ylab="Frecuencia")
hist(x=Otra$Trihalomethanes, main="THM en el agua, Otra region",
     xlab="THM", ylab="Frecuencia")

par(mfrow=c(1,2))
hist(x=Maule$Turbidity, main="Turbidez del agua, Maule",
     xlab="Turbidez", ylab="Frecuencia")
hist(x=Otra$Turbidity, main="Turbidez del agua, Otra region",
     xlab="Turbidez", ylab="Frecuencia")

par(mfrow=c(1,2))
hist(x=Maule$Potability, main="Potabilidad del agua, Maule",
     xlab="Potabilidad", ylab="Frecuencia")
hist(x=Otra$Potability, main="Potabilidad del agua, Otra region",
     xlab="Potabilidad", ylab="Frecuencia")
```

### Variables cualitativas
```{r}
par(mfrow=c(1,2))
plot(x=data$city, main="Ciudades", xlab="Ciudad", ylab="Frecuencia")
plot(x=data$area, main="Area de la ciudad", xlab="Area", ylab="Frecuencia")

summary(data$city)
summary(data$area)
```

### Distribucion de datos, Variables cuantitativas
```{r}
library(ggplot2)

x=data$time.emergency
ggplot(data) +
  geom_density(aes(x=x), fill='steelblue')+ xlab("Tiempo de emergencia") + ylab("Frecuencia")+
  ggtitle("Distribucion de la variable time.emergency (tiempo de emergencia)")+
  theme_minimal()

x=data$type.emergency
ggplot(data) +
  geom_density(aes(x=x), fill='steelblue')+ xlab("Tipo de emergencia") +   ylab("Frecuencia")+
  ggtitle("Distribucion de la variable type.emergency (tipo de emergencia)")+
  theme_minimal()

x=data$ph
ggplot(data) +
  geom_density(aes(x=x), fill='steelblue')+xlab("pH") +   ylab("Frecuencia")+
  ggtitle("Distribucion de la variable ph")+
  theme_minimal()

x=data$Hardness
ggplot(data) +
  geom_density(aes(x=x), fill='steelblue')+xlab("Hardness") +   ylab("Frecuencia")+
  ggtitle("Distribucion de la variable Hardness (Dureza)")+
  theme_minimal()

x=data$Solids
ggplot(data) +
  geom_density(aes(x=x), fill='steelblue')+xlab("Solids") +   ylab("Frecuencia")+
  ggtitle("Distribucion de la variable Solids (Solidos)")+
  theme_minimal()

x=data$Chloramines
ggplot(data) +
  geom_density(aes(x=x), fill='steelblue')+xlab("Chloramines") +   ylab("Frecuencia")+
  ggtitle("Distribucion de la variable Chloramines (Cloro)")+
  theme_minimal()

x=data$Sulfate
ggplot(data) +
  geom_density(aes(x=x), fill='steelblue')+xlab("Sulfate") +   ylab("Frecuencia")+
  ggtitle("Distribucion de la variable Sulfate (Sulfato)")+
  theme_minimal()

x=data$Conductivity
ggplot(data) +
  geom_density(aes(x=x), fill='steelblue')+xlab("Conductivity") +   ylab("Frecuencia")+
  ggtitle("Distribucion de la variable Conductivity (Conductividad)")+
  theme_minimal()

x=data$Organic_carbon
ggplot(data) +
  geom_density(aes(x=x), fill='steelblue')+xlab("Organic_carbon") +   ylab("Frecuencia")+
  ggtitle("Distribucion de la variable Organic_carbon (Carbon organico)")+
  theme_minimal()

x=data$Trihalomethanes
ggplot(data) +
  geom_density(aes(x=x), fill='steelblue')+xlab("Trihalomethanes") +   ylab("Frecuencia")+
  ggtitle("Distribucion de la variable Trihalomethanes (THM)")+
  theme_minimal()

x=data$Turbidity
ggplot(data) +
  geom_density(aes(x=x), fill='steelblue')+xlab("Turbidity") +   ylab("Frecuencia")+
  ggtitle("Distribucion de la variable Turbidity (Turbidad)")+
  theme_minimal()

x=data$Potability
ggplot(data) +
  geom_density(aes(x=x), fill='steelblue')+xlab("Potability") +   ylab("Frecuencia")+
  ggtitle("Distribucion de la variable Potability (Potabilidad)")+
  theme_minimal()
```

### Outliers, valores fuera de rango
```{r}
boxplot(data$time.emergency)
outliers <- boxplot.stats(data$time.emergency)$out
outliers

boxplot(data$type.emergency)
outliers <- boxplot.stats(data$type.emergency)$out
outliers

boxplot(data$ph)
outliers <- boxplot.stats(data$ph)$out
outliers

boxplot(data$Hardness)
outliers <- boxplot.stats(data$Hardness)$out
outliers

boxplot(data$Solids)
outliers <- boxplot.stats(data$Solids)$out
outliers

boxplot(data$Chloramines)
outliers <- boxplot.stats(data$Chloramines)$out
outliers

boxplot(data$Sulfate)
outliers <- boxplot.stats(data$Sulfate)$out
outliers

boxplot(data$Conductivity)
outliers <- boxplot.stats(data$Conductivity)$out
outliers

boxplot(data$Organic_carbon)
outliers <- boxplot.stats(data$Organic_carbon)$out
outliers

boxplot(data$Trihalomethanes)
outliers <- boxplot.stats(data$Trihalomethanes)$out
outliers

boxplot(data$Turbidity)
outliers <- boxplot.stats(data$Turbidity)$out
outliers

boxplot(data$Potability)
outliers <- boxplot.stats(data$Potability)$out
outliers
```



